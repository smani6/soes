from src.manager import Manager
from src.main import Request

if __name__ == "__main__":

    stock_list = ["buy abc 10", "sell xyz 15", "sell abc 13"]

    req_obj = Request()
    for stock_order in stock_list:
        req_obj.format_req(stock_order)

    req = req_obj.get_req()

    entities = Manager().process_stocks(req)
    for entity in entities:
        print entity.side, entity.company, entity.quantity, entity.rem_quantity, entity.status
