import pytest
from _pytest.monkeypatch import monkeypatch
from src.entity import Stock
from src.manager import Manager

@pytest.fixture
def fixture_entity():
    data = Stock()
    data.company = "abc"
    data.quantity = 10
    data.rem_quantity = 10
    data.side = "buy"
    return data

class TestManager(object):

    def test_process_stock_initial_one_order(self):

        req = [{'side': "buy", "company": "abc", "quantity": 10}]
        orders_list = Manager().process_stocks(req)
        assert orders_list[0].status == "Open"

    def test_process_stock_initial_mutiple_orders_same_company(self):
        req = [{'side': "buy", "company": "abc", "quantity": 10},{'side': "sell", "company": "abc", "quantity": 10}]
        orders_list = Manager().process_stocks(req)
        assert orders_list[0].status == "Closed"

    def test_process_stock_initial_mutiple_orders_diff_company(self):
        req = [{'side': "buy", "company": "abc", "quantity": 10}, {'side': "sell", "company": "xyz", "quantity": 10}]
        orders_list = Manager().process_stocks(req)
        assert orders_list[0].status == "Open"

    def test_process_stock_initial_mutiple_orders_diff_company_same_die(self):
        req = [{'side': "buy", "company": "abc", "quantity": 10}, {'side': "buy", "company": "abc", "quantity": 10}]
        orders_list = Manager().process_stocks(req)
        assert orders_list[0].status == "Open"

    def test_process_stock_initial_mutiple_orders_diff_company_same_die(self):

        req1 = [{'side': "sell", "company": "abc", "quantity": 10}]
        mgr_obj = Manager()
        orders_list = mgr_obj.process_stocks(req1)
        entity_obj = fixture_entity
        print entity_obj.company
        is_prev_order_exits, prev_orders = mgr_obj.check_if_prev_order_exits(entity_obj)
        assert is_prev_order_exits == True





