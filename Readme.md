Stock Order Execution System

Steps to run the problem:
 
   1. Run the main.py file inside src folder. You will be prompted to enter the stock orders , once entered press enter to execute the orders and output will be displayed in the console itself.
   2. Once outputs displayed, you will be asked whether you want to enter more orders, Press y(yes) and enter more orders and press enter at the last to execute the orders.
   3. Outputs will be displayed in the console.
