class Stock(object):

    """
        Entity class - containing stock order
        attributes.
    """
    def __init__(self):

        self.__side = None
        self.__company = None
        self.__quantity = None
        self.__rem_quantity = None
        self.__status = None

    @property
    def side(self):
        return self.__side

    @side.setter
    def side(self, value):
        self.__side = value

    @property
    def company(self):
        return self.__company

    @company.setter
    def company(self, value):
        self.__company = value

    @property
    def quantity(self):
        return self.__quantity

    @quantity.setter
    def quantity(self, value):
        self.__quantity = value

    @property
    def rem_quantity(self):
        return self.__rem_quantity

    @rem_quantity.setter
    def rem_quantity(self, value):
        self.__rem_quantity = value

    @property
    def status(self):
        return self.__status

    @status.setter
    def status(self, value):
        self.__status = value
