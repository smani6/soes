from manager import Manager

class Request(object):

    """
        Class to form Request
        from the input value entered by user, form a dict
        and append into req list which would be sent to manager
        to process the request

    """
    def __init__(self):
        self.req = []

    def format_req(self,val):
        d = {}
        splitted_values = val.split()
        d['side'] = splitted_values[0]
        d['company'] = splitted_values[1]
        d['quantity'] = int(splitted_values[2])
        self.req.append(d)

    def print_req(self):
        print self.req

    def get_req(self):
        return self.req

if __name__ == "__main__":

    """
        Main Function to start the script
        Input stock orders are obtained from console
        and the orders are formatted and passed to
        manager class to process the orders.
        The returned output is displayed on the console
    """
    text = ""
    stopword = ""
    req_gen_obj = Request()
    initial = "y"

    while initial == "y":
        print "Enter Stocks orders in below format"
        print "Side Company Quantity (separated by spaces)"
        print "Enter mutiple stock orders in separate line"
        print "Once Stock orders entered - press enter again to execute"

        while True:
            line = raw_input()
            if line.strip() == stopword:
                break
            req_gen_obj.format_req(line)

        req = req_gen_obj.get_req()

        entities = Manager().process_stocks(req)

        print "Output \n"
        for entity in entities:
            print entity.side, entity.company, entity.quantity, entity.rem_quantity, entity.status

        print "\nNeed to place more stock orders ?? Enter y or n"
        initial = raw_input()