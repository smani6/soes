from entity import Stock
import config as config

class Manager(object):
    """
        Manager class to process the input stock orders
        The orders are processed according to the strategy
        and order status changed according to it and returned
    """
    _instance = None

    def __new__(cls, *args, **kwargs):

        """
            Override new method to create singleton manager instance

        """
        if not cls._instance:
            cls._instance = super(Manager, cls).__new__(cls, *args, **kwargs)
        return cls._instance

    def __init__(self):
        self.entire_orders = []

    def process_stocks(self, req):

        """
            Method to process the input stock orders
            and return the orders with status
            Pseudo_code:
                Input Req is passed to form_entities method
                to form stock objects
                Each obj is iterated to check any prev_order exits
                If exits and the conditions matches current and prev
                order status changed accordingly
                else order added to the list

        :param req: stock_order - dict
        :return: Entire stock orders list
        """
        try:
            entity_list = self.form_entities(req)

            for obj in entity_list:
                is_prev_order_exits, prev_orders = self.check_if_prev_order_exits(obj)
                if is_prev_order_exits:
                    self.strategy(obj,prev_orders)
                else:
                    obj.status = config.OPEN
                    obj.rem_quantity = obj.quantity
                    self.entire_orders.append(obj)

            return self.entire_orders
        except Exception as e:
            print e

    def check_if_prev_order_exits(self, obj):
        """
            Method to check any previous stock order of same company
            with status to open and of opposite side (buy to sell and vice versa)
        :param obj: stock_order object
        :return: is_prev_order_exits - boolean, prev_orders - list containing
                matching prev_orders
        """
        try:
            prev_orders = []
            is_prev_order_exits = False
            for order in self.entire_orders:
                if obj.company == order.company and order.status == config.OPEN and obj.side != order.side:
                    prev_orders.append(order)
                    is_prev_order_exits = True

            return is_prev_order_exits, prev_orders

        except Exception as e:
            print e

    def form_entities(self,req):

        """
            Method to form stock objects from input dict req
        :param req: stock_order request dict
        :return: list containing stock_objects
        """
        entity_list = []
        for each_req in req:
            entity_obj = Stock()
            for key, value in each_req.iteritems():
                setattr(entity_obj, key, value)
            entity_obj.rem_quantity = entity_obj.quantity
            entity_list.append(entity_obj)

        return entity_list

    def strategy(self,obj,prev_orders):
        """
            Strategy method to apply conditions to each stock order
            and change the status of current and prev orders
            Pseudo-code:
                For each stock order - if the previous order of same company
                is not closed - remaining quantity is calculated
                if rem_quantity = 0 : then current and prev order status changed to closed
                elif rem_quantity < 0 : previous order status changed to closed
                    and current order - rem_quantity = rem_value and status to open
                else if rem_quan > 0 : previous order rem_quantity = rem_value and status to
                    open and current order quantity to 0 and status to closed
        :param obj: current stock_order obj
        :param prev_orders: list containing prev_stock_orders of same company
        :return: void
        """
        for order in prev_orders:
            if obj.status != config.CLOSED:
                rem = order.rem_quantity - obj.quantity
                if rem == 0:
                    order.rem_quantity = 0
                    order.status = config.CLOSED
                    obj.rem_quantity = abs(rem)
                    obj.status = config.CLOSED

                elif rem < 0:
                    order.rem_quantity = 0
                    order.status = config.CLOSED
                    obj.rem_quantity = abs(rem)
                    obj.status = config.OPEN

                else:
                    order.rem_quantity = rem
                    order.status = config.OPEN
                    obj.rem_quantity = 0
                    obj.status = config.CLOSED

        self.entire_orders.append(obj)